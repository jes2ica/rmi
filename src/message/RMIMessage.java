package message;

import java.io.Serializable;

import server.Remote;



public interface RMIMessage extends Remote, Serializable {
	
	public Object get();
	
	public void set(Object obj);

}
