package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import message.RMIMessage;

public class ServerHelper implements Runnable {
	Socket socket;
	MainServer server;
	ObjectInputStream in = null;
	ObjectOutputStream out = null;

	public ServerHelper(Socket socket, MainServer server) throws IOException {
		this.socket = socket;
		this.server = server;
		in = new ObjectInputStream(socket.getInputStream());
		out = new ObjectOutputStream(socket.getOutputStream());
	}

	
	/**
	 * The thread continuously listens to the specific client and get the message sent by client.
	 */
	
	@Override
	public void run() {

		try {
			while(true) {
				RMIMessage message = (RMIMessage)in.readObject();
				server.processMessage(message, socket.getInetAddress().toString(), socket.getPort());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}


}
