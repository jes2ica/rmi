package message;

public class MethodInvocationMessage implements RMIMessage{
	
	private static final long serialVersionUID = -6064142633792325851L;
	private int objectID;
	private long methodHashValue;
	Object[] params;
	
	public int getObjectID() {
		return objectID;
	}
	public void setObjectID(int objectID) {
		this.objectID = objectID;
	}
	public long getMethodHashValue() {
		return methodHashValue;
	}
	public void setMethodHashValue(long methodHashValue) {
		this.methodHashValue = methodHashValue;
	}
	public Object[] getParams() {
		return params;
	}
	public void setParams(Object[] params) {
		this.params = params;
	}
	
	@Override
	public Object get() {
		return this;
	}

	@Override
	public void set(Object obj) {
	}
}
