package server;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ClientHelper {

	private RemoteRef rod;

	public ClientHelper(RemoteRef rod) {
		this.rod = rod;
	}

	public Remote findStub () {
		String subName = rod.getObjectClassName() + "_Stub";
		Remote stub = null;
		try {
			Class<?> stubClass = Class.forName(subName);
			Constructor<?>[] cons = stubClass.getConstructors();
			stub = (Remote) cons[0].newInstance();	
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			stub = askForStub();
		}
		return stub;
	}
	
	public Remote askForStub() {
		return null;
	}


}
