package registry;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import exception.RemoteException;
import exception.RemoteException.Type;
import message.ExceptionMessage;
import message.RMIMessage;
import message.RegistryMessage;
import message.RegistryMessage.Operation;
import message.ReturnMessage;
import server.RemoteRef;
import util.Util;

public class RegistryServer implements Runnable, Registry {

	private static final int DEFAULT_PORT = 1099;
	private int port;
	private ServerSocket listener;
	private static final long serialVersionUID = 4751638760612530381L;
	private static volatile HashMap<String, RemoteRef> instanceList = new HashMap<String, RemoteRef>();


	public RegistryServer(int port) {
		this.port = port;
	}

	@Override
	public RemoteRef lookup(String name) {
		if(!instanceList.containsKey(name)) {
			RemoteException e = new RemoteException(Type.NoSuchInstance, new Throwable("The serveer doesn't have such instance!"));
			ExceptionMessage em = new ExceptionMessage();
			em.set(e);
			return null;
		}
		return instanceList.get(name);
	}

	@Override
	public void rebind(String name, RemoteRef ref) {
		synchronized(instanceList) {
			instanceList.put(name, ref);
		}
	}

	@Override
	public String[] list() {
		String[] names = null;
		synchronized(instanceList) {
			names = new String[instanceList.size()];
			int i = 0;
			for(String name : instanceList.keySet()) {
				names[i++] = name;
			}
		}
		return names;
	}

	@Override
	public void run() {
		try {
			listener = new ServerSocket(port);
		} catch (IOException e) {
			System.err.println("Can not create a server!");
		}

		while(true) {
			Socket socket;
			try {
				socket = listener.accept();
				Thread service = new Thread(new RegistryService(socket, this));
				service.start();
			} catch (IOException e) {
				System.err.println("Can not create a connection between the client and the server!");
			}
		}

	}

	public void processMessage (RMIMessage message, String host, int port) {

		if(message instanceof RegistryMessage) {
			RegistryMessage rm = (RegistryMessage) message.get();
			
			if(rm.getOperation() == Operation.LOOKUP) {
				RemoteRef rr = lookup(rm.getName());
				ReturnMessage rtm = new ReturnMessage();
				rtm.set(rr);
				Util.sendMessage(host, port, (RMIMessage) rtm);
				return;
			} 
			
			if (rm.getOperation() == Operation.LIST) {
				String[] names = list ();
				ReturnMessage rtm = new ReturnMessage();
				rtm.set(names);
				Util.sendMessage(host, port, (RMIMessage) rtm);
				return;

			} 
			
			if (rm.getOperation() == Operation.REBIND) {
				synchronized(instanceList) {
					instanceList.put(rm.getName(), rm.getRr());
				}
				return;
			}
		
		} else {
			RemoteException e = new RemoteException(Type.WrongMessage, new Throwable("The server cannot handle such message!"));
			ExceptionMessage em = new ExceptionMessage();
			em.set(e);
			Util.sendMessage(host, port, (RMIMessage)em);
		}
	}


	public static void main (String args[]) {
		RegistryServer server;
		if(args.length < 1) {
			server = new RegistryServer(DEFAULT_PORT);
		} else {
			server = new RegistryServer(Integer.parseInt(args[0]));
		}
		Thread serverThread = new Thread(server);
		serverThread.start();	
	}

}
