package exception;

import java.io.IOException;

public class RemoteException extends IOException {
	
	private static final long serialVersionUID = 8845669616785991137L;
	public Throwable cause;
	private Type type;
	
	public enum Type {
		NoSuchMethod,
		NoSuchInstance,
		WrongMessage
	}
	
	public RemoteException(Type type, Throwable cause) {
		super(type.toString());
		initCause(null);
        this.cause = cause;
    }
	
	public Throwable getCause() {
		return cause;
	}

	public void setCause(Throwable cause) {
		this.cause = cause;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
}
