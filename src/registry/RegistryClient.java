package registry;

import server.RemoteRef;
import util.Util;
import message.RMIMessage;
import message.RegistryMessage;
import message.RegistryMessage.Operation;

public class RegistryClient implements Registry {
	
	private static final long serialVersionUID = 4867198059237040685L;
	private String host;
	private int port;
	
	public RegistryClient(String host, int port) {
		this.host = host;
		this.port = port;
	}
	@Override
	public RemoteRef lookup(String name) {
		RegistryMessage rm = new RegistryMessage(null, null, Operation.LOOKUP);
		Util.sendMessage(host, port, (RMIMessage) rm);
		RMIMessage message = Util.receiveMessage(host, port);
		return (RemoteRef) message.get();
	}

	@Override
	public void rebind(String name, RemoteRef ref) {
		RegistryMessage rm = new RegistryMessage(name, ref, Operation.REBIND);
		Util.sendMessage(host, port, (RMIMessage) rm);
	}

	@Override
	public String[] list() {
		RegistryMessage rm = new RegistryMessage(null, null, Operation.LIST);
		Util.sendMessage(host, port, (RMIMessage) rm);
		RMIMessage message = Util.receiveMessage(host, port);
		return (String[]) message.get();
	}

}
