package message;

public class GetStubMessage implements RMIMessage{
	
	private static final long serialVersionUID = 5637892793938222465L;
	private String host;
	private int port;
	private String objectClassName;
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getObjectClassName() {
		return objectClassName;
	}
	public void setObjectClassName(String objectClassName) {
		this.objectClassName = objectClassName;
	}
	
	@Override
	public Object get() {
		return this;
	}

	@Override
	public void set(Object obj) {
	}
	
	
	

}
