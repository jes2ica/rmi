package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;

import registry.RegistryClient;
import exception.RemoteException;
import exception.RemoteException.Type;
import util.Util;
import message.ExceptionMessage;
import message.GetStubMessage;
import message.MethodInvocationMessage;
import message.RMIMessage;
import message.ReturnMessage;

public class MainServer implements Runnable{

	private static final int DEFAULT_PORT = 15640;
	private int port;
	private int objectID;
	private ServerSocket listener;
	private volatile HashMap <Integer, Remote> instanceList = new HashMap<Integer, Remote>();
	private volatile HashMap <Integer, HashMap <Long, Method>> methodList = new HashMap <Integer, HashMap <Long, Method>>();

	public MainServer(int port) {
		this.port = port;
	}

	@Override
	public void run() {
		try {
			listener = new ServerSocket(port);
		} catch (IOException e) {
			System.err.println("Can not create a server!");
		}

		while(true) {
			Socket socket;
			try {
				socket = listener.accept();
				Thread service = new Thread(new ServerHelper(socket, this));
				service.start();
			} catch (IOException e) {
				System.err.println("Can not create a connection between the client and the server!");
			}
		}
	}

	/**
	 * This method is responsible for processing the message sent by client.
	 * Message types can be : 
	 * 
	 * (1) GetStubMessage -> The client may ask the server for the content of the specific stub class.
	 * (2) MethodInvocationMessage -> The client may ask the server to invoke specific method.
	 * 
	 * @param message
	 */
	
	public void processCommand () {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String command;
		String args[];
		
		while(true) {
			try {
				command = reader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
			
			args = command.split(" ");
			
			if(args.length == 0) {
				System.err.println("Please provide correct command!");
				continue;
			}
			if(args.length == 1 && args[0].equals("quit")) {
				System.exit(0);
			}
			if(args[0].equals("register")) {
				if(args.length == 3) {
					String className = args[1];
					String aliasName = args[2];
					
					try {
						Class<?> implClass = Class.forName(className);
						if(!(Remote.class.isAssignableFrom(implClass))) {
							System.err.println("The object is not a remote obejct!");
							continue;
						}
						Constructor<?>[] cons = implClass.getConstructors();
						Remote instance = (Remote) cons[0].newInstance();
						String localIP = InetAddress.getLocalHost().getHostAddress().toString();
						RemoteRef rr = new RemoteRef(localIP, port, objectID, aliasName);
						synchronized(instanceList) {
							instanceList.put(objectID++, instance);
						}
						RegistryClient client = new RegistryClient(localIP, port);
						client.rebind(aliasName, rr);
						System.out.println("The server has sent the registry information to the registry server !");
						continue;
						
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (UnknownHostException e) {
						e.printStackTrace();
					}
				}
					
					
 				else{
					System.out.println("Usage: register ImplmentedClassName InstanceName");
					continue;
				}
			}
		}
	}
	
	
	public void processMessage (RMIMessage message, String host, int port) {

		if(message instanceof GetStubMessage) {

			GetStubMessage gsm = (GetStubMessage) message.get();
			byte[] classContent = Util.readFile(gsm.getObjectClassName() +"_Stub.class");
			ReturnMessage rm = new ReturnMessage();
			rm.set(classContent);
			Util.sendMessage(host, port, (RMIMessage) rm);
			System.out.println("The server has sent the content of the stub class to the client !");
			return;

		} 
		
		if (message instanceof MethodInvocationMessage) {

			MethodInvocationMessage mis  = (MethodInvocationMessage) message.get();
			long methodHashCode = mis.getMethodHashValue();
			int instanceID = mis.getObjectID();
			Object instance = instanceList.get(instanceID);

			if(instance == null) {
				RemoteException e = new RemoteException(Type.NoSuchInstance, new Throwable("The server doesn't have such instance!"));
				ExceptionMessage em = new ExceptionMessage();
				em.set(e);
				Util.sendMessage(host, port, (RMIMessage)em);
				return;
			}

			Method method = methodList.get(mis.getObjectID()).get(methodHashCode);
			Object[] params = mis.getParams();

			if(method == null) {
				RemoteException e = new RemoteException(Type.NoSuchMethod, new Throwable("The instance doesn't have such method!"));
				ExceptionMessage em = new ExceptionMessage();
				em.set(e);
				Util.sendMessage(host, port, (RMIMessage)em);
				return;
			} 

			try {
				if(method.getReturnType().equals(void.class)) {
					method.invoke(instance, params);
					System.out.println(method.getName()+" method invoked by instance "+instanceID+" has been executed");
					return;
				}
				
				Object returnValue = method.invoke(instanceList.get(mis.getObjectID()), params);
				ReturnMessage rm = new ReturnMessage();
				rm.setReturnInfo(returnValue);
				System.out.println(method.getName()+" method invoked by instance "+instanceID+" has been executed");
				Util.sendMessage(host, port, (RMIMessage)rm);
				
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		} else {
			
			RemoteException e = new RemoteException(Type.WrongMessage, new Throwable("The server cannot handle such message!"));
			ExceptionMessage em = new ExceptionMessage();
			em.set(e);
			Util.sendMessage(host, port, (RMIMessage)em);
		}
	}

	public static void main (String args[]) {
		MainServer server;
		if(args.length < 1) {
			server = new MainServer(DEFAULT_PORT);
		} else {
			server = new MainServer(Integer.parseInt(args[0]));
		}
		Thread serverThread = new Thread(server);
		serverThread.start();	
		server.processCommand();
	}
}
