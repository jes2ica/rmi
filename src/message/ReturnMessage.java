package message;

public class ReturnMessage implements RMIMessage{
	
	private static final long serialVersionUID = -2920287837082768640L;
	Object returnInfo;

	public Object getReturnInfo() {
		return returnInfo;
	}

	public void setReturnInfo(Object returnInfo) {
		this.returnInfo = returnInfo;
	}
	
	@Override
	public Object get() {
		return returnInfo;
	}

	@Override
	public void set(Object obj) {
		this.returnInfo = obj;
	}
	

}
