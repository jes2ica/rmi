package registry;

import java.io.Serializable;

import server.Remote;
import server.RemoteRef;

public interface Registry extends Remote, Serializable {
	
	public RemoteRef lookup(String name);
	
	public void rebind(String name, RemoteRef ref);
	
	public String[] list();
	
}
