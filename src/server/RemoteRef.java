package server;

import java.io.Serializable;

public class RemoteRef implements Serializable{
	
	private static final long serialVersionUID = 5163325410199044171L;
	private String host;
	private int port;
	private int objectID;
	private String objectClassName;
	
	public RemoteRef(String host, int port, int objectID, String objectClassName) {
		this.host = host;
		this.port = port;
		this.objectID = objectID;
		this.objectClassName = objectClassName;
	}
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getObjectID() {
		return objectID;
	}
	public void setObjectID(int objectID) {
		this.objectID = objectID;
	}
	public String getObjectClassName() {
		return objectClassName;
	}
	public void setObjectClassName(String objectClassName) {
		this.objectClassName = objectClassName;
	}	
}
