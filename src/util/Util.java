package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;

import message.RMIMessage;

public class Util {
	
	private static volatile HashMap<String, Socket> socketList = new HashMap<String, Socket>();
	
	
	public static void sendMessage (String host, int port, RMIMessage message) {
		
		String identifier = host + port;
		try {
			if(!socketList.containsKey(identifier)) {
				Socket socket;
				socket = new Socket(host, port);
				synchronized(socketList) {
					socketList.put(identifier, socket);
				}
			} else {
				Socket socket = socketList.get(identifier);
				ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
				out.writeObject(message);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static RMIMessage receiveMessage (String host, int port) {
		
		String identifier = host + port;
		RMIMessage message = null;
		try {
			if(!socketList.containsKey(identifier)) {
				Socket socket;
				socket = new Socket(host, port);
				synchronized(socketList) {
					socketList.put(identifier, socket);
				}
			} else {
				Socket socket = socketList.get(identifier);
				ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
				message = (RMIMessage) in.readObject();
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return message;
	}
	
	public static byte[] readFile (String fileName) {
		File file = new File(fileName);
		byte[] fileContent = new byte[(int)file.length()];
		FileInputStream fis = null;
		
		try {
			fis = new FileInputStream(file);
			fis.read(fileContent);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return fileContent;
	}
	
	
	public static void writeFile (String fileName, byte[] fileContent) {
		
		FileOutputStream fos = null;
		try {
			fos= new FileOutputStream(fileName);
			fos.write(fileContent);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static long computeMethodHash(Method m) {
		long hashCode = m.getName().hashCode() ^ m.getReturnType().hashCode();
		for(Class<?> params : m.getParameterTypes()) {
			hashCode ^= params.hashCode();
		}	
		return hashCode;
	}

}
